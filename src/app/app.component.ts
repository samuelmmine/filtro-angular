import { Component } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'filtro';
  searchText: string;

  objects = [{nome: 'Ricardo', idade: 12}, 
  {nome: 'Pedro', idade: 12},
  {nome: 'Fernando', idade: 13},
  {nome: 'Maria', idade: 14}, 
  {nome: 'José', idade: 14}, 
  {nome: 'Vera Lúcia', idade: 14}, 
  {nome: 'Paloma', idade: 14}, 
  {nome: 'Alexandre', idade: 14}, 
  {nome: 'Severino', idade: 14}, 
  {nome: 'Marlene', idade: 14}, 
  {nome: 'Milton', idade: 14}, 
  {nome: 'Carlos', idade: 14}, 
  {nome: 'Carla', idade: 14},
  {nome: 'Carolina', idade: 14}, 
  {nome: 'Caramelo', idade: 14},  
  {nome: 'Samuel', idade: 15}
];

clearInput() {
  this.searchText = '';
}

}
