import { Pipe, PipeTransform } from '@angular/core';
import { items } from './items.model';

@Pipe({
  name: 'filter'
})
export class FilterPipe implements PipeTransform {
  transform(items: items[], searchText: string): items[] {
   
    if(!items) return [];
    if(!searchText) return items;

    searchText = searchText.toLowerCase();
    return items.filter( it => {
        return it.nome.toLowerCase().includes(searchText);
        });
    }
}